﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WellDone.JwtAuthConfig.Options
{
    internal static class AuthorisationSettings
    {
        internal static class AdditionalOptions
        {
            public static string ISSUER = "MyAuthServer";
            public static string AUDIENCE = "MyAuthClient";
            public static string KEY = "E786CE0F-B056-4526-9A12-130A1A0A1D39";
            public static int LIFETIME = 43200;
        }

        internal static class JwtBearerOptions
        {
            public static bool RequireHttpsMetadata = false;
            public static bool ValidateIssuer = true;
            public static bool ValidateAudience = true;
            public static bool ValidateLifetime = true;
            public static bool ValidateIssuerSigningKey = true;
        }
    }
}
