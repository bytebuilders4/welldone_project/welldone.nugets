﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WellDone.JwtAuthConfig.Options
{
    public class AuthOptions
    {
        public const string Position = "Authorization:AditionalOptions";
        /// <summary>
        /// Издатель токена
        /// </summary>
        public string Issuer { get; set; }
        /// <summary>
        /// Потребитель токена
        /// </summary>
        public string Audience { get; set; }
        /// <summary>
        /// Ключ для шифрования
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// Время жизни токена в минутах
        /// </summary>
        public int LifeTime { get; set; }
    }
}
