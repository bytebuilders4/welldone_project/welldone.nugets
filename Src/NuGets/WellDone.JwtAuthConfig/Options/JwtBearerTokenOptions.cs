﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WellDone.JwtAuthConfig.Options
{
    public class JwtBearerTokenOptions
    {
        public const string Position = "Authorization:JwtBearerOptions";

        /// <summary>
        /// Возвращает или задает значение , если для адреса или центра метаданных требуется протокол HTTPS
        /// </summary>
        public bool RequireHttpsMetadata { get; set; }

        /// <summary>
        /// Валидация издателя токена
        /// </summary>
        public bool ValidateIssuer { get; set; }

        /// <summary>
        /// Валидация потребителя токена
        /// </summary>
        public bool ValidateAudience { get; set; }

        /// <summary>
        /// Валидация времеги существования
        /// </summary>
        public bool ValidateLifetime { get; set; }

        /// <summary>
        /// Валидация ключа безопасности
        /// </summary>
        public bool ValidateIssuerSigningKey { get; set; }
    }
}
