﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using WellDone.JwtAuthConfig.Options;

namespace WellDone.JwtAuthConfig.Extensions
{
    public static class AuthExtensions
    {
        public static IServiceCollection AddJWTAuthentification(this IServiceCollection services)
        {
            var authOptions = new AuthOptions()
            {
                Audience = AuthorisationSettings.AdditionalOptions.AUDIENCE,
                Issuer = AuthorisationSettings.AdditionalOptions.ISSUER,
                Key = AuthorisationSettings.AdditionalOptions.KEY,
                LifeTime = AuthorisationSettings.AdditionalOptions.LIFETIME,
            };

            var jwtOptions = new JwtBearerTokenOptions()
            {
                RequireHttpsMetadata = AuthorisationSettings.JwtBearerOptions.RequireHttpsMetadata,
                ValidateIssuer = AuthorisationSettings.JwtBearerOptions.ValidateIssuer,
                ValidateAudience = AuthorisationSettings.JwtBearerOptions.ValidateAudience,
                ValidateLifetime = AuthorisationSettings.JwtBearerOptions.ValidateLifetime,
                ValidateIssuerSigningKey = AuthorisationSettings.JwtBearerOptions.ValidateIssuerSigningKey,
            };

            services.AddSingleton(authOptions);
            services.AddAuthentication(authOpt =>
            {
                authOpt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                authOpt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                    .AddJwtBearer(jwtOpt =>
                    {
                        jwtOpt.RequireHttpsMetadata = jwtOptions.RequireHttpsMetadata;
                        jwtOpt.TokenValidationParameters = new TokenValidationParameters
                        {
                            // укзывает, будет ли валидироваться издатель при валидации токена
                            ValidateIssuer = jwtOptions.ValidateIssuer,
                            // будет ли валидироваться потребитель токена
                            ValidateAudience = jwtOptions.ValidateAudience,
                            // будет ли валидироваться время существования
                            ValidateLifetime = jwtOptions.ValidateLifetime,
                            // валидация ключа безопасности
                            ValidateIssuerSigningKey = jwtOptions.ValidateIssuerSigningKey,
                            // строка, представляющая издателя
                            ValidIssuer = authOptions.Issuer,
                            // установка потребителя токена
                            ValidAudience = authOptions.Audience,
                            // установка ключа безопасности
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(authOptions.Key))
                        };
                    });

            return services;
        }

        public static IServiceCollection AddCustomAuthorization(this IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy("AdminPolicy", policy =>
                {
                    policy.RequireRole("Admin");
                });
                options.AddPolicy("UserPolicy", policy =>
                {
                    policy.RequireAuthenticatedUser();
                });
            });

            return services;
        }
    }
}
