﻿namespace WellDode.MassTransitModels
{
    public class CreateUserMessage
    {
        public required Guid Id { get; set; }
        public required string Username { get; set; }
        public required string Email { get; set; }
    }
}
