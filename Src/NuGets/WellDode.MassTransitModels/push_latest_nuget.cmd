set source=Bin\Debug\

FOR /F "eol=| delims=" %%I IN ('DIR "%source%*.nupkg" /A-D /B /O-D /TW 2^>nul') DO (
    SET "NewestFile=%%I"
    GOTO FoundFile
)
ECHO No *.nupkg file found!
GOTO :EOF

:FoundFile
ECHO Newest *.nupkg file is: "%NewestFile%"

ECHO Try to push in repo

dotnet nuget push %source%%NewestFile% -s WdGitLab
